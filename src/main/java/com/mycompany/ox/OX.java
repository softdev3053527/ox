/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ox;

import java.util.Scanner;

/**
 *
 * @author slmfr
 */
public class OX {
    private char[][] board;
    private char Player1;
    private char Player2;
    private char turn;
    
    public OX(){
        board = new char[3][3];
        Player1 = 'X';
        Player2 = 'O';
        turn = Player1;
        CreateBoard();
    }
    
    private void CreateBoard(){
        for(int row = 0; row < 3 ;row++){
            for(int col = 0 ; col < 3 ;col++){
                board[row][col] = '-';
            }
        }
    }
    
    private void display(){
        
        for(int row = 0; row < 3 ;row++){
            System.out.println(" -------------");
            for(int col = 0 ; col < 3 ;col++){
                if(col%1 ==  0){
                    System.out.print(" | ");
                }
                System.out.print(board[row][col]);
            }
            System.out.println(" | ");
        }
        System.out.println(" -------------");
        System.out.println("Turn " + turn + " move");
    }
    
    private void switchTurn(){
        if(turn == Player1){
            turn = Player2;
        }else{
            turn = Player1;       
        }
    }
    
    private  void play(){
        
        Scanner kb = new Scanner(System.in);
        boolean gameFinish = false;
        while (!gameFinish) {
            display();
            System.out.println("input row and col : ");
            int row = kb.nextInt()-1;
            int col = kb.nextInt()-1;
            if(isValidMove(row, col) && !isBoardFull() && !isVictory()){
                makeMove(row, col);
                
                if(isVictory()){
                    display();
                    System.out.println( "Player "+ turn + " wins!");
                    System.out.println("input 1 to restart or 2 to exit :");
                    int option = kb.nextInt();
                    if(option == 1){                     
                        CreateBoard();
                        continue;
                    }else if(option == 2 ){
                        gameFinish = true;
                    }
                }
                if(isBoardFull()){
                    display();
                    System.out.println("It's a draw!");
                    System.out.println("input 1 to restart or 2 to exit :");
                    int option = kb.nextInt();
                if(option == 1){
                    
                    CreateBoard();
                    continue;
                }else if(option == 2 ){
                    gameFinish = true;
                    }
            }
                switchTurn();
            }else{
                System.out.println("nope");
            }
            
            
            
        }



    }
    
    private void makeMove(int row,int col){
        board[row][col] = turn;
    }
    
    private boolean isValidMove(int row ,int col){
        return row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == '-';
    }
    
    private  boolean isVictory(){
        for (int row = 0; row < 3; row++) {
            if (board[row][0] == turn && board[row][1] == turn && board[row][2] == turn) {
                return true;
            }
        }
        
        // Check columns
        for (int col = 0; col < 3; col++) {
            if (board[0][col] == turn && board[1][col] == turn && board[2][col] == turn) {
                return true;
            }
        }
        
        // Check diagonals
        if ((board[0][0] == turn && board[1][1] == turn && board[2][2] == turn) || (board[0][2] == turn && board[1][1] == turn && board[2][0] == turn)) {
            return true;
        }
        
        return false;
    
    }
    
        private boolean isBoardFull() {
            for (int row = 0; row < 3; row++) {
                for (int col = 0; col < 3; col++) {
                    if (board[row][col] == '-') {
                        return false;
                    }
                }
            }

            return true;
    }

    public static void main(String[] args) {
        OX game = new OX();
        game.play();

        
        
    }
}
